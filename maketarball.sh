#!/usr/bin/env bash

if [[ $# -ne 2 ]]; then
        echo USAGE: $0 name version
        exit 1
fi
cd files
tar czf ../tar/${1}-${2}.tar.gz *
