import QtQuick 2.6
import Sailfish.Silica 1.0

ApplicationWindow {
    id: app
    initialPage: Component { Page {
      anchors.fill: parent
      SilicaFlickable {
        anchors.fill: parent
        contentHeight: col.height
      Column {
        id: col
        width: parent.width
        spacing: Theme.itemSizeLarge
        anchors.horizontalCenter: parent.horizontalCenter
        Row {
          spacing: Theme.itemSizeSmall
          anchors.horizontalCenter: parent.horizontalCenter
          Button { text: "Button" ; preferredWidth: Theme.buttonWidthSmall }
          Button { text: "Button" ; icon.source: "image://theme/icon-m-ambience" }
          IconButton { icon.source: "image://theme/icon-m-ambience" }
        }
        Row {
          spacing: Theme.itemSizeSmall
          anchors.horizontalCenter: parent.horizontalCenter
          TestButton { text: "TestButton" ; preferredWidth: Theme.buttonWidthSmall }
          TestButton { text: "Button" ; icon.source: "image://theme/icon-m-ambience" }
          TestButton { icon.source: "image://theme/icon-m-ambience" }
        }
        Row {
          spacing: Theme.itemSizeSmall
          anchors.horizontalCenter: parent.horizontalCenter
          OrigButton { text: "OrigButton"; preferredWidth: Theme.buttonWidthSmall  }
          OrigButton { text: "Button" ; icon.source: "image://theme/icon-m-ambience" }
        }
        Button {     text: "Button"; preferredWidth: Theme.buttonWidthLarge }
        OrigButton { text: "Button"; preferredWidth: Theme.buttonWidthLarge }
        TestButton { text: "Button"; preferredWidth: Theme.buttonWidthLarge }
        IconButton { icon.source: "image://theme/icon-m-ambience" }
      }
      }
    }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
